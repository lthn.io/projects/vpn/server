# flake8: noqa

"""
    Lethean VPN

    Distributed Virtual Private Marketplace  # noqa: E501
    Contact: contact@lethean.io
"""


__version__ = "1.0.0"